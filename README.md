# govhack2018

https://github.com/edwinsteele/d3-projects/tree/master/data

au-states.geojson: map of Australian states from the 2006 Census. Data from 
  http://www.abs.gov.au/ausstats/abs@.nsf/DetailsPage/1259.0.30.0022006?OpenDocument
  and processed in QGIS using simplify geometries with a tolerance of 0.005

